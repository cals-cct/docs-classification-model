### People

Lead: 

- Matt King, CCT

Collaborators: 

- Hong Cui, UA School of Information
- Dongfang Xu, UA School of Information

Data Scientist: 

- Kristina Riemer, CCT

### Summary

The purpose of the Rangelands Gateway project is to make it easier to search for rangeland research documents. One component of this project is the Range Docs search engine that improves the process of searching through rangelands publications and papers. The contribution of this incubator project, as specified in [the original proposal](https://docs.google.com/document/d/1FszoqK1HtIZjBv6MBeRx9tlIwNZHMiyY-AbjdXgperc/edit#heading=h.7acew6bm10nj), to Range Docs is an algorithm to classify these documents with a list of common rangelands terms, with the algorithm created using a set of documents that have already been annotated by hand by experts in the field. 

We generated a classification model for each annotation term in the list, then used resampling of the dataset to determine the quality of each term’s model. This process resulted in the straightforward recommendation that each annotation term needs a minimum of 200 annotation instances before using the model to classify new text, as this was the minimum required input to get a model of reasonable quality. 

### Project organization

#### Folder and file structure

- `clean_data` folder
    - `all_tagged_annotations.csv` file: annotated text data cleaned into tabular format
- `figures` folder
    - `test_eval_metrics.png`: figure of evaluation metrics for testing data model runs
    - `train_eval_metrics.png`: figure of evaluation metrics for training data model runs
- `model_fits` folder: model fits for all models (100 `.rds` files)
- `raw_data` folder: annotated text documents data (10 `.json` files)
- `scripts` folder
    - `clean_annotations.R`: script to clean raw `.json` data into clean `.csv`
    - `download_data.sh`: shell script to download raw `.json` data using elasticdump
    - `model_annotations.R`: script to create and evaluate models for all tags, and plot evaluation metrics
- `README.md`: a description of the project, including results and recommendations

#### Cleaned data file

| Column name   | Description                                                  |
|:---------------|:--------------------------------------------------------------|
| page          | unique id for document and page of annotated data            |
| document      | unique id for document of annotated data                     |
| annotation    | second unique id for page of annotated data                  |
| annotation_id | unique id for each text selection                            |
| text          | third unique id for page of annotated data                   |
| text_contents | content of text that was annotated                           |
| tag           | unique number for each tag associated with a particular text |
| tags_contents | tag associated with that text                                |
| num           | number for which raw data file annotation came from          |

### Methods

The data for this project consisted of a set of 15 rangeland research documents, which were domain-specific research papers. These were read by experts who assigned annotation terms to each paragraph of the document. These terms came from a pre-generated list of over one thousand words or phrases. These data were provided in a JSON file. 

All scripts used to clean and model these data are in the associated [repository](https://bitbucket.org/cals-cct/docs-classification-model), and should be reproducible following the steps described below. The workflow combines command line tools and R packages including elasticdump, ndjson, dplyr, and tidymodels. The data file, which had a complicated nested structure, was downloaded with download_data.sh and cleaned into a tidy format in `clean_annotations.R`. This resulted in each annotation tag having its own dataset, which consisted of each paragraph that had been annotated and whether or not that paragraph had been tagged with that term. For example, the term “Livestock” was associated with 167 paragraphs out of a total of 2,456 paragraphs. 

A classification model was created for each annotation in `model_annotations.R` and then evaluated. First, each term’s dataset was split into test and training datasets, with three quarters of the data allocated to the training dataset. The testing and training datasets had similar proportions of paragraphs with the annotation term. 

#### Data preparation

In order to prepare data for modeling, each annotation term’s training dataset was preprocessed using these steps: 

1. As described in the previous section, the initial dataset consisted of all the paragraphs that had been annotated from 15 documents and whether or not the annotation term in question was associated with each paragraph by experts. 
2. Each paragraph was split into individual words, with each word being considered a 'token' in this analysis. 
3. Stop words were removed. Stop words are words that have little meaning such as “and”, "or", and "but".
4. The 500 most common tokens were retained for each paragraph.
5. Each token was weighted based on its frequency in that paragraph and how many times it appeared across all of the other paragraphs, which is referred to as “term frequency-inverse document frequency.” 
6. The final dataset for each annotation term was all of the tokenized paragraphs associated with the term, and an equal number of randomly selected paragraphs that did not have the term. 

#### Analysis

The model fitting and analysis was done independently for each annotation term in the following steps:

1. A classification model was fit using a support vector machine (SVM) with a polynomial kernel. This SVM approach maximizes the difference between the locations of the two categories of data, term present or absent, in a multi-dimensional space, and specifically the distance is determined non-linearly with the polynomial kernel. 
2. Each tag’s model performance was evaluated using cross validation based on four metrics. These metrics are described below in “Model Performance Metrics”. 
    a. Each annotation term’s training dataset was split into 10 randomly-determined groups, or 'folds'. 
    b. The model was fit to nine of the ten of the folds. 
    c. Performance was evaluated using the held-out fold by predicting whether the annotation term should be associated with each paragraph and then comparing that to whether it actually was. 
    d. Steps b. and d. were repeated for each fold, and the model performance metrics were averaged across the ten folds.
3. Each tag’s model performance was also evaluated by running the models on the withheld testing data and calculating the metrics in “Model Performance Metrics” except for AUC ROC. 

#### Model performance metrics

The four evaluation metrics are described below. For all metrics, a value closer to one indicates better model performance. 

1. Accuracy: proportion of held-out data that has presence or absence of annotation term correctly predicted
2. Sensitivity: proportion of held-out data that had the annotation term associated with it that was predicted correctly
3. Specificity: proportion of held-out data that did not have the annotation term associated with it that was predicted correctly
4. AUC ROC: assesses model performance at difference thresholds, also known as area under the curve of the receiver operator characteristic. The receiver operator characteristic compares true positive to false positive rate by doing sensitivity to (1 - specificity). This set of threshold comparisons is summarized by calculating the area under the curve of this characteristic. Values near 0.5 indicate predictions are no better than random guesses

We also compare these model evaluation metrics to those for a null model. The same resampling folds are used, but the null model predicts that the annotation term is associated with each paragraph. We then expect accuracy and specificity values to be near zero and sensitivity to be near one. 

Model fits were saved as `.rds` files to the project’s repository for later use in determining terms for new text documents. 

### Results 

We fit and evaluated models for the most commonly annotated 100 annotation terms. The terms and the number of times they’re annotated are shown below. 

| Terms                   | Occurrences | Terms                            | Occurrences |
|:-------------------------|:-------------|:----------------------------------|:-------------|
| Monitoring (rangelands) | 712         | Heterogeneity                    | 98          |
| Rangeland Health        | 473         | Total annual production          | 97          |
| Sampling Design         | 458         | Management Objectives            | 96          |
| Climate                 | 382         | Land Use Planning (rangelands)   | 95          |
| Rangeland               | 345         | Supplement (rangelands)          | 92          |
| Ecosystem               | 323         | Monitoring Objective             | 91          |
| Grazing Management      | 268         | Ecological Resilience            | 90          |
| Erosion                 | 259         | indicator                        | 90          |
| Livestock Management    | 246         | Random Sampling                  | 90          |
| Livestock               | 237         | Woody                            | 90          |
| Riparian Ecosystems     | 232         | Rangeland Inventory (rangelands) | 89          |
| Global Rangelands       | 229         | Grassland                        | 88          |
| Management Objective    | 224         | Precipitation                    | 87          |
| Ecological Site         | 221         | Wildlife                         | 87          |
| Soil                    | 221         | Cover                            | 85          |
| Monitoring              | 217         | Bare Ground                      | 84          |
| Facilitating practices  | 213         | Density                          | 84          |
| Sampling Unit           | 204         | Nutrition (rangelands)           | 84          |
| Drouth (Drought)        | 202         | Species Composition              | 81          |
| Sample Size             | 198         | Transition pathway               | 81          |
| Rangeland Hydrology     | 196         | Sampling                         | 80          |
| Anti-quality chemicals  | 189         | Lignin                           | 78          |
| Riparian Zone           | 182         | Plant Community                  | 78          |
| Range Management        | 159         | Plant Structure                  | 78          |
| socioeconomic           | 159         | Key Area                         | 76          |
| Livestock Production    | 158         | Grazing Preference               | 74          |
| Trend                   | 158         | Management Plan                  | 74          |
| Seasonal Use            | 157         | vegetation                       | 74          |
| protocol                | 155         | Litter (rangelands)              | 72          |
| na                      | 154         | Population                       | 72          |
| Resilience              | 152         | Quadrats                         | 72          |
| Indicator               | 150         | available forage                 | 71          |
| Ecology                 | 147         | Vegetative management practices  | 71          |
| Frequency               | 138         | forage intake                    | 68          |
| Forage Production       | 135         | Forage utilization               | 66          |
| Remote Sensing          | 135         | Rotational Stocking              | 66          |
| Key Species             | 125         | Spot Grazing                     | 66          |
| Range Research          | 125         | Grazing (rangelands)             | 64          |
| Soil map unit           | 123         | woody                            | 64          |
| Pasture                 | 117         | Disturbance                      | 63          |
| Adaptive Management     | 108         | Threshold                        | 63          |
| Monitoring Objectives   | 108         | Browse                           | 62          |
| soil stability          | 108         | Habitat (rangelands)             | 62          |
| Tannin                  | 108         | Nutrient management              | 61          |
| Forage (rangelands)     | 105         | Range Degradation                | 61          |
| Invasion                | 105         | Winter Range                     | 61          |
| Qualitative Monitoring  | 103         | detoxification                   | 60          |
| Recovery period         | 102         | Forage Allowance                 | 60          |
| Monitoring Plan         | 100         | Quantitative Monitoring          | 60          |
| Grazing Distribution    | 98          | Range Administration             | 60          |

Model performance metrics for these 100 annotation terms are shown in the figure below in panel A. Mean values from cross-validation are shown as black points, and the corresponding null models are grey points. Grey dashed lines are for reference, showing 200 annotation occurrences and 0.75 values for metrics. Most terms with more than 200 occurrences had metric values exceeding 0.75, though the sensitivity value for seven of the terms with more than 200 occurrences were below that metric cutoff. 

These models were also evaluated after running them on the testing data, then comparing the model predictions to the tagged data itself. The evaluation metrics are shown in the figure below in panel B as black points. Grey dashed lines are for reference, showing 200 annotation occurrences and 0.75 values for metrics. Most terms with more than 200 occurrences had metric values exceeding 0.75. 

![](figures/eval_metrics.png)

### Recommendations and next steps

Based on the model evaluations, we recommend that each annotation term should have a minimum of 200 annotations in order to predict their potential occurrence in new documents. We believe sensitivity is the most important model metric, as it is more crucial for documents to be returned with whether they have the term as more important than false negatives. 

Possible next steps for different parts of this project are listed below. 

- Data cleaning
    - Review the annotations list for similar terms that could be combined and terms that do not have a clear definition
    - Remove annotations with NA
- Possible model improvements
    - Add more features and do feature selection
    - Tune hyperparameters C and gamma using grid search
    - Build a single model for all tags. This is not currently supported by the R tidymodels infrastructure. The tool fastText has been suggested as an approach. 
- Incorporate model into Rangeland Docs software to improve search
- Use models to prioritize further annotation based on model skill and classification uncertainty for each unannotated paragraph.
- Rerun model pipeline once annotation terms have been cleaned up
- Automate rerunning model pipeline at chosen time intervals or when there is sufficient new annotated data

### Acknowledgements

Dr. Hong Cui provided guidance on modeling approaches. 

### References

Hvitfeldt, Emil, and Julia Silge. 2021. Supervised Machine Learning for Text Analysis in R. CRC Press. [URL](https://smltar.com/)
