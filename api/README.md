## Rangeland Docs API

### How to run

run `entrypoint.R` script

pop-up window with Swagger specs
can test by clicking "POST" button, then "try it out" button and pasting in the example dataset from `example_text_data.json` file to the "New data to predict" section

this returns, under "Response body" section, each input text with the associated tags; text with no associated tags isn't returned




