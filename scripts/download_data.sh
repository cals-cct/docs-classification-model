#!/bin/sh
password=`cat password.txt`

elasticdump \
    --input=https://resource_guest:$password@es.rangelandsgateway.org:9200/resource_production_v2 \
    --output=/Users/kristinariemer/Dropbox/Documents/UofA/Projects/Incubator/RangeDocs/docs-classification-model/raw_data/new_elasticdump.json \
    --type=data \
    --maxRows=1000